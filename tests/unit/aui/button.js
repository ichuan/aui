'use strict';

import '../../../src/js/aui/dropdown2';
import '../../../src/js/aui/button';
import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import skate from '../../../src/js/aui/internal/skate';
import keyCode from '../../../src/js/aui/key-code';

var button;

describe('aui/button', function () {
    describe('- button group', function () {
        describe('with a dropdown2 trigger adds and removes the aui-dropdown2-in-buttons class', function () {
            function expectButtonsClass(dropdown, isExpected, summary) {
                expect(dropdown.classList.contains('aui-dropdown2-in-buttons')).to.equal(isExpected, summary);
            }

            let trigger;
            let dropdown;

            beforeEach(function () {
                const fixtures = helpers.fixtures({
                    buttons: `<div class="aui-buttons">
                            <button id="trigger-button" class="aui-button aui-dropdown2-trigger" aria-haspopup="true" aria-owns="test-dropdown">Dropdown trigger</button>
                        </div>`,
                    dropdown: `<div id="test-dropdown" class="aui-style-default aui-dropdown2">
                                <ul><li><a href="#">Test item</a></li></ul>
                            </div>`
                });
                trigger = fixtures.buttons.querySelector('#trigger-button');
                dropdown = fixtures.dropdown;
                skate.init(trigger);
                skate.init(fixtures.dropdown);
            });

            it('to dropdown triggered via click', function() {
                expectButtonsClass(dropdown, false, 'Dropdown should not have class before triggered');
                helpers.click(trigger);
                expectButtonsClass(dropdown, true, 'Dropdown should have class when triggered by click');
                helpers.click(trigger);
                expectButtonsClass(dropdown, false, 'Dropdown should not have class when closed by click');
            });

            it('to dropdown triggered via keyboard', function() {
                expectButtonsClass(dropdown, false, 'Dropdown should not have class before triggered');
                trigger.focus();
                helpers.pressKey(keyCode.SPACE);
                expectButtonsClass(dropdown, true, 'Dropdown should have class when triggered by keyboard');
                helpers.pressKey(keyCode.ESCAPE);
                expectButtonsClass(dropdown, false, 'Dropdown should not have class when closed by keyboard');
            });
        });
    });

    describe('- button', function () {
        beforeEach(function () {
            button = helpers.fixtures({
                button: '<button class="aui-button"></button>'
            }).button;
            skate.init(button);
        });

        afterEach(function () {
            $(button).remove();
        });

        it('global', function () {
            expect(AJS.button).to.equal(undefined);
        });

        it('AMD module', function (done) {
            amdRequire(['aui/button'], function (amdModule) {
                expect(amdModule).to.equal(AJS.button);
                done();
            });
        });

        it('Button element has prototype', function () {
            expect(button.busy).to.be.a('function');
            expect(button.idle).to.be.a('function');
        });

        it('Calling busy should set aria-busy', function () {
            button.busy();

            expect(button.getAttribute('aria-busy')).to.equal('true');
        });

        it('Calling busy should add spin container inside button', function () {
            button.busy();
            expect(button.querySelectorAll('.spinner').length).to.equal(1);
        });

        it('Calling busy twice should only add one spin container inside button', function () {
            button.busy();
            button.busy();
            expect(button.querySelectorAll('.spinner').length).to.equal(1);
        });

        it('Calling idle should unset aria-busy', function () {
            button.busy();
            button.idle();

            expect(button.hasAttribute('aria-busy')).to.be.false;
        });

        it('Calling idle should remove spin container from inside button', function () {
            button.busy();
            button.idle();
            expect(button.querySelectorAll('.aui-button-spinner').length).to.equal(0);
        });

        it('Calling isBusy returns false initially', function () {
            expect(button.isBusy()).to.be.false;
        });

        it('Calling isBusy returns true when busy', function () {
            button.busy();
            expect(button.isBusy()).to.be.true;
        });

        it('Calling isBusy returns false when idled', function () {
            button.busy();
            button.idle();
            expect(button.isBusy()).to.be.false;
        });
    });
});
