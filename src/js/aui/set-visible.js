'use strict';

import $ from './jquery';
import { fn as deprecateFn } from './internal/deprecation';
import globalize from './internal/globalize';

/**
 * Shortcut function adds or removes 'hidden' classname to an element based on a passed boolean.
 *
 * @param {String | Element} element The element or an ID to show or hide.
 * @param {boolean} show true to show, false to hide.
 *
 * @returns {undefined}
 */
function setVisible (element, show) {
    if (!(element = $(element))) {
        return;
    }

    $(element).each(function () {
        var isHidden = $(this).hasClass('hidden');

        if (isHidden && show) {
            $(this).removeClass('hidden');
        } else if (!isHidden && !show) {
            $(this).addClass('hidden');
        }
    });
}

var setVisible = deprecateFn(setVisible, 'setVisible', {
    sinceVersion: '5.9.0',
    extraInfo: 'No alternative will be provided. Use jQuery.addClass() / removeClass() instead.'
});

globalize('setVisible', setVisible);

export default setVisible;
