'use strict';

import $ from './jquery';
import { fn as deprecateFn } from './internal/deprecation';
import globalize from './internal/globalize';

/**
 * Shortcut function to see if passed element is currently visible on screen.
 *
 * @param {String | Element} element The HTMLElement or an jQuery selector to check.
 *
 * @returns {Boolean}
 */
function isVisible (element) {
    return !$(element).hasClass('hidden');
}

var isVisible = deprecateFn(isVisible, 'isVisible', {
    sinceVersion: '5.9.0',
    extraInfo: 'No alternative will be provided. Use jQuery.hasClass() instead.'
});

globalize('isVisible', isVisible);

export default isVisible;
