'use strict';
import skate from 'skatejs';

var auiSkate = skate.noConflict();

export default auiSkate;
