var path = require('path');
var header = require('gulp-header');
var pkg = require(path.join(__dirname, '..', '..', 'package.json'));

var banner = ['/*!',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @license <%= pkg.license %>',
  ' * @author <%= pkg.author %>',
  ' */',
  ''].join('\n');

module.exports = function() {
    return header(banner, {pkg: pkg});
};
