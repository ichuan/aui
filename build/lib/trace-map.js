var path = require('path');

module.exports = function (base) {
    return {
        // This is required because Backbone imports "jquery" and that will import
        // the entire jQuery source if we don't tell it to use our shim instead.
        jquery: path.join(base, 'src', 'js', 'aui', 'jquery.js'),

        // Once we don't have modified sources we can remove this to resolve them
        // from our node_modules. For now, this fixes the issue when we require
        // Backbone it tries to require Underscore using its module name rather
        // than its path. This would then normally look in the node_modules
        // directory, but the map forces it to resolve to a path.
        //
        // This also lets consumers of the `lib/` build use the versions of underscore
        // and backbone specified in their `package.json`.
        backbone: path.join(base, 'src', 'js-vendor', 'backbone', 'backbone.js'),
        underscore: path.join(base, 'src', 'js-vendor', 'underscorejs', 'underscore.js')
    };
};
